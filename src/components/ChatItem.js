import { Flex,Box, Text } from "@chakra-ui/layout";
import { useEffect, useState } from "react";
import { ChatState } from "../Context/ChatProvider";

const ChatItem = (props) => {
  const [isOnline, setisOnline] = useState(false);
  
  const { selectedChat, setSelectedChat ,onlineUsers } = ChatState();

  useEffect(() => {
    const index = onlineUsers.findIndex(obj =>obj.userId === props.SenderId);
    if(index > -1){
        console.log("userFound", props.Sender)
        setisOnline(true);
   }
   else if(index === -1){
    setisOnline(false);
   }

    // eslint-disable-next-line
  }, [onlineUsers]);

  const mystyle = {
   
    backgroundColor:selectedChat === props.chatFound ?  "white" : "#38B2AC" ,
    width:"10px",
    height:"10px",
    borderRadius: "50%",
    
  
  };

return(

    <Box
    onClick={() => setSelectedChat(props.chatFound)}
    cursor="pointer"
    bg={selectedChat === props.chatFound ? "#38B2AC" : "#E8E8E8"}
    color={selectedChat === props.chatFound ? "white" : "black"}
    px={3}
    py={2}
    borderRadius="lg">
    <Flex
     justify="space-between"
     align="center">
    <Text>
   {props.Sender}
    </Text>
    
 {isOnline ? <div style={mystyle}></div> : " "}
    </Flex>
  
  </Box>

)
}

export default ChatItem;