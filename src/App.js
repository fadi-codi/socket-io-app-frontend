import './App.css';
import Homepage from "./Pages/Homepage";
import {BrowserRouter, Route,  Routes } from "react-router-dom";
import Chatpage from "./Pages/Chatpage";

function App() {
  return (
    <div className="App">
      <BrowserRouter>
      <Routes>
      <Route path="/" element={<Homepage/>}  />
      <Route path="/chats" element={<Chatpage/>} />
      </Routes>
      </BrowserRouter>
    </div>
  );
}

export default App;
